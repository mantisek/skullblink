# What is it

It's a python script that uses the standard library to rapidly change the background of a 64-bit windows hosts.

# Usage

Drop and run.

`python3 skullblink.py`

Make sure your images are in the same directory.

