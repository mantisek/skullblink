import os, time
import ctypes


def get_image_file_list():
    pass

def nt_change_background(filepath):
    wallpaper = bytes(filepath,'utf-8')
    ctypes.windll.user32.SystemParametersInfoA(20, 0, wallpaper, 3)


def main():

    if os.name == "nt":
        current_dir = os.getcwd()
        folder = r"{}".format(current_dir)
        file_one = r"redskull.PNG"
        file_two = r"blackscreen.png"
        file_list = [file_one, file_two]
        while True:
            for i in file_list:
                time.sleep(0.01)
                full_path = os.path.join(folder, i)
                nt_change_background(full_path)
    else:
        exit()

main()